<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LoginTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $user = [
            'name' => 'Bahtiar Rivai' . date('YmdHis'),
            'email' => 'bahtiar' . date('YmdHis') . '@gmail.com',
            'phone' => '082424' . date('His'),
            'credit' => 0,
            'password' => 'passwordtest',
            'password_confirmation' => 'passwordtest',
            'roles' => 'regular-user'
        ];
        $this->post('/api/register', $user);

        $dataLogin = [
            'phone' => $user['phone'],
            'password' => $user['password']
        ];

        $response = $this->post('/api/login', $dataLogin);

        $response->assertSuccessful();

    }
}
