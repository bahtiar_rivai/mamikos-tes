<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RegisterTest extends TestCase
{

    /** @test */
   public function a_user_can_register()
   {
       $user = [
           'name' => 'Bahtiar Rivai' . date('YmdHis'),
           'email' => 'bahtiar'  . date('YmdHis') . '@gmail.com',
           'phone' => '08242' . date('YmdHis'),
           'credit' => 0,
           'password' => 'passwordtest',
           'password_confirmation' => 'passwordtest',
           'roles' => 'premium-user'
       ];

       $response = $this->post('/api/register', $user);

       $response->assertSuccessful();
   }
}
