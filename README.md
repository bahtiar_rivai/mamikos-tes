## About Test Mamikos
- Laravel Version 8.17
- Clone Repository Git
- cp .env.example to .env
- edit .env change setting database config
- composer install
- php artisan key:generate
- php artisan migrate
- php artisan passport:install
- php artisan db:seed --class=RolesAndPermissionsSeeder
