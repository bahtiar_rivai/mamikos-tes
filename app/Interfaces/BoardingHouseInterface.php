<?php

namespace App\Interfaces;

use App\Http\Requests\BoardingHouseRequest;
use App\Http\Requests\BoardingHouseAskRequest;

interface BoardingHouseInterface
{
    /**
     * Get all boarding-house
     *
     * @method  GET api/boarding-house
     * @access  public
     */
    public function getAllPaginate();

    /**
     * Get BoardingHouse By ID
     *
     * @param   integer     $id
     *
     * @method  GET api/boarding-house/{id}
     * @access  public
     */
    public function getBoardingHouseById($id);

    /**
     * Create boardingHouse
     *
     * @param   \App\Http\Requests\BoardingHouseRequest    $request
     * @param   integer                           $id
     *
     * @method  POST    api/boarding-house       For Create
     * @method  PUT     api/boarding-house/{id}  For Update
     * @access  public
     */
    public function store(BoardingHouseRequest $request);

    /**
     * Create boardingHouse
     *
     * @param   \App\Http\Requests\BoardingHouseAskRequest    $request
     *
     * @access  public
     */
    public function ask(BoardingHouseAskRequest $request);

    /**
     * Create | Update boardingHouse
     *
     * @param   \App\Http\Requests\BoardingHouseRequest    $request
     * @param   integer                           $id
     *
     * @method  POST    api/boarding-house       For Create
     * @method  PUT     api/boarding-house/{id}  For Update
     * @access  public
     */
    public function requestBoardingHouse(BoardingHouseRequest $request, $id = null);

    /**
     * Delete boardingHouse
     *
     * @param   integer     $id
     *
     * @method  DELETE  api/boarding-house/{id}
     * @access  public
     */
    public function deleteBoardingHouse($id);
}
