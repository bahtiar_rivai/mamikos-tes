<?php

namespace App\Interfaces;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\OtpRequest;
use App\Http\Requests\RegisterRequest;

interface UserInterface
{

    /**
     * Get User By ID
     *
     * @param   integer     $id
     *
     * @method  GET api/users/{id}
     * @access  public
     */
    public function getUserById($id);

    /**
     * Signin/Login user
     *
     * @param   \App\Http\Requests\LoginRequest    $request
     *
     * @method  POST    api/login       For Login
     * @access  public
     */
    public function requestLogin(LoginRequest $request);

    /**
     * Signup/Register user
     *
     * @param   \App\Http\Requests\RegisterRequest    $request
     *
     * @method  POST    api/register       For Register
     * @access  public
     */
    public function requestRegister(RegisterRequest $request);

    /**
     * Submit OTP
     *
     * @param   \App\Http\Requests\OtpRequest    $request
     *
     * @method  POST    api/submit-otp       For Submit OTP
     * @access  public
     */
    public function requestSubmitOTP(OtpRequest $request);

}
