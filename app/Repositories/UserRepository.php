<?php

namespace App\Repositories;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\OtpRequest;
use App\Http\Requests\RegisterRequest;
use App\Http\Resources\UserResource;
use App\Interfaces\UserInterface;
use App\Models\User;
use App\Traits\ApiResponser;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserRepository implements UserInterface
{
    // Use ApiResponser Trait in this repository
    use ApiResponser;

    public function getUserById($id)
    {
        try {
            $user = User::find($id);

            // Check the user
            if(!$user) return $this->error("No user with ID $id", 404);

            return $this->success("User Detail", $user);
        } catch(\Exception $e) {
            return $this->error($e->getMessage(), $e->getCode());
        }
    }


    public function requestLogin(LoginRequest $request)
    {
        try {
            if(Auth::attempt(['phone' => $request->phone, 'password' => $request->password])){
                $user   = Auth::user();
                $token  = $user->createToken('Mamikos-password-client')->accessToken;
                return $this->successResponse(['data' => $user, 'token' => $token]);
            }
            else{
                return $this->errorResponse('Unauthorised', 401);
            }
        } catch(\Exception $e) {
            return $this->errorResponse('Unauthorised', 401);
        }
    }


    public function requestRegister(RegisterRequest $request)
    {
        DB::beginTransaction();
        try {

            $data                   = $request->all();
            unset($data['roles']);
            unset($data['password_confirmation']);
            $data['password']       = bcrypt($data['password']);
            $data['credit']         = ($request->input('roles') == 'premium-user') ? 40 :  (($request->input('roles') == 'regular-user') ? 20 : 0);
            $data['status']         = 'unverified';
            $data['otp']            = rand(100000, 999999);
            $user                   = User::create($data);
            $user->assignRole($request->input('roles'));

            $token = $user->createToken('Mamikos password client')->accessToken;
            $data = new UserResource($user);

            DB::commit();
            return $this->successResponse(['data' => $data, 'token' => $token]);

        } catch(\Exception $e) {
            DB::rollBack();
            return $this->errorResponse('Register failed', 422);
        }
    }


    public function requestSubmitOTP(OtpRequest $request)
    {
        DB::beginTransaction();
        try {

            $user = User::where('otp', $request->input('otp'))->first();
            if(!$user){
                return $this->errorResponse('OTP Failed', 401);
            }
            $user->otp                  = $request->input('otp');
            $user->status               = 'verified';
            $user->phone_verified_at    = date('Y-m-d H:i:s');
            $user->save();

            DB::commit();
            return $this->successResponse(['data' => $user]);
        } catch(\Exception $e) {
            DB::rollBack();
            return $this->errorResponse('Unauthorised', 401);
        }
    }
}
