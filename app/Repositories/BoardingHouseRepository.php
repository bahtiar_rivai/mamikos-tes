<?php

namespace App\Repositories;

use App\Interfaces\BoardingHouseInterface;
use App\Traits\ApiResponser;
use App\Models\BoardingHouse;
use App\Models\BoardingHousePrice;
use App\Models\BoardingHouseImage;
use App\Models\BoardingHouseAsk;
use App\Http\Requests\BoardingHouseRequest;
use App\Http\Requests\BoardingHouseAskRequest;
use App\Http\Resources\BoardingHouseResource;
use App\Http\Resources\BoardingHouseAskResource;
use Illuminate\Database\Eloquent\Builder;
use DB;

class BoardingHouseRepository implements BoardingHouseInterface
{

    // Use ApiResponser Trait in this repository
    use ApiResponser;

    public function getAllPaginate($args = [])
    {
        // Set default args
        $args = array_merge([
            'perPage' => 9,
            'name' => false,
            'sortBy' => null,
            'page' => 2
        ], $args);

        try {
            $query = BoardingHouse::query();
            $query->with('prices', 'images');

            if (isset($args['min_price'])):
                $query->whereHas('prices', function (Builder $q) use ($args) {
                    $q->where('price', '<=', $args['min_price']);
                });
            endif;

            if (isset($args['max_price'])):
                $query->whereHas('prices', function (Builder $q) use ($args) {
                    $q->where('price', '<=', $args['max_price']);
                });
            endif;

            if (isset($args['name'])):
                $query->where('name', 'like', '%'.$args['name'].'%');
            endif;

            if (isset($args['address'])):
                $query->where('address', 'like', '%'.$args['address'].'%');
            endif;

            $query->orderBy(BoardingHousePrice::select('price')
                  ->whereColumn('boarding_houses.id', 'boarding_house_prices.boarding_house_id'), 'asc');

            $boardingHouses = $query->paginate($args['perPage'], ['*'], 'page', $args['page']);
            $data = BoardingHouseResource::collection($boardingHouses->items());
            return $this->paginationResponse($data, $boardingHouses);
        } catch(\Exception $e) {
            return $this->errorResponse($e->getMessage(), 404);
        }
    }

    public function getBoardingHouseById($id)
    {
        try {
            $boardingHouse = BoardingHouse::find($id);
            if(!$boardingHouse) return $this->errorResponse("No boardingHouse with ID $id", 404);
            $boardingHouse = new BoardingHouseResource($boardingHouse);
            return $this->successResponse($boardingHouse, "BoardingHouse Detail");
        } catch(\Exception $e) {
            return $this->errorResponse($e->getMessage(), 404);
        }
    }

    public function store(BoardingHouseRequest $request)
    {
        DB::beginTransaction();
        try {

        $data               = $request->all();
        unset($data['prices']);
        unset($data['images']);
        $data['facilities'] = json_encode($data['facilities']);
        $data['owner_id']   = auth()->user()->id;
        $boardingHouse = BoardingHouse::create($data);
        foreach($request->file('images') as $k => $v):
            $getimageName = time().'.'.$v->getClientOriginalExtension();
            $image = $v->move(public_path('images'), $getimageName);
            BoardingHouseImage::create(['boarding_house_id' => $boardingHouse->id,
                                        'file' => $getimageName]);
        endforeach;
        foreach($request->input('prices') as $k => $v):
            BoardingHousePrice::create(['boarding_house_id' => $boardingHouse->id,
                                        'type_price' => $v['type'],
                                        'price' => $v['price']]);
        endforeach;

        $data   = new BoardingHouseResource($boardingHouse);

        DB::commit();
        return $this->successResponse(['data' => $data]);

        } catch(\Exception $e) {
            DB::rollBack();
            return $this->errorResponse($e->getMessage(), $e->getCode());
        }
    }

    public function ask(BoardingHouseAskRequest $request)
    {

        DB::beginTransaction();
        try {
        $boardingHouse = BoardingHouse::find($request->input('boarding_house_id'));
        if(!$boardingHouse) return $this->errorResponse("No user with ID $id", 404);

        $data['ask']                = $request->input('ask');
        $data['boarding_house_id']  = $boardingHouse->id;
        $data['ask_to']             = $boardingHouse->owner_id;
        $data['ask_from']           = auth()->user()->id;
        $boardingHouseAsk = BoardingHouseAsk::create($data);

        $data   = new BoardingHouseAskResource($boardingHouseAsk);

        DB::commit();
        return $this->successResponse(['data' => $data]);

        } catch(\Exception $e) {
            DB::rollBack();
            return $this->errorResponse($e->getMessage(), 404);
        }

    }

    public function requestBoardingHouse(BoardingHouseRequest $request, $id = null)
    {
    }

    public function deleteBoardingHouse($id)
    {
    }
}
