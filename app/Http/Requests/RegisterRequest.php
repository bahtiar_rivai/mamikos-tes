<?php

namespace App\Http\Requests;

use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                  => 'required',
            'password'              => 'required|min:6',
            'password_confirmation' => 'required_with:password|same:password|min:6',
            'email'                 => 'required|email|unique:users',
            'phone'                 => 'required|unique:users',
        ];
    }

    public function messages()
    {
        return [
            'name.required'                         => 'Nama wajib diisi.',
            'password.required'                     => 'Password wajib diisi.',
            'password.min'                          => 'Password minimal diisi dengan 5 karakter.',
            'password_confirmation.required_with'   => 'Konfirmasi password wajib diisi dan harus cocok dengan password.',
            'password_confirmation.same'            => 'Konfirmasi password dan password harus cocok.',
            'password_confirmation.min'             => 'Konfirmasi password minimal diisi dengan 5 karakter.',
            'email.required'                        => 'Email wajib diisi.',
            'email.email'                           => 'Email tidak valid.',
            'email.unique'                          => 'Email sudah terdaftar.',
            'phone.required'                        => 'Phone wajib diisi.',
            'phone.unique'                          => 'Phone sudah terdaftar.'
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();

        throw new HttpResponseException(
            response()->json([
                'status'=>'Error',
                'type_message' => 'array',
                'message' => $errors,
                'meta' => null,
                'code' => 422
            ], 422)
        );
    }

}
