<?php

namespace App\Http\Requests;

use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest;

class BoardingHouseAskRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'boarding_house_id'     => 'required',
            'ask'                   => 'required',
        ];
    }

    public function messages()
    {
        return [
            'boarding_house_id.required'            => 'Kost wajib diisi.',
            'ask.required'                          => 'Tanya wajib diisi.',
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        $meta = [
            'type_message'  => 'array',
            'message'       => $errors,
            'code'          => 422
        ];
        throw new HttpResponseException(
            response()->json([
                'status'        => false,
                'meta'          => $meta,
            ], 422)
        );
    }

}
