<?php

namespace App\Http\Requests;

use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest;

class BoardingHouseRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                  => 'required',
            'address'               => 'required|min:20',
            'type_kos'              => 'required',
            'type_owner'            => 'required',
            'resident'              => 'required',
            'room_size'             => 'required',
            'year_built'            => 'required',
            'number_of_rooms'       => 'required',
            'number_of_empty_rooms' => 'required',
            'facilities'            => 'required',
            'other_facilities'      => 'required',
            'bathroom'              => 'required',
            "prices"                => ["required","array"],
            "images"                => ["required","array","min:2","max:8"],
            "images.*"              => ["required","mimes:jpeg,jpg,png,gif"],
        ];
    }

    public function messages()
    {
        return [
            'name.required'                         => 'Nama kost wajib diisi.',
            'address.required'                      => 'Alamat wajib diisi.',
            'address.min'                           => 'Alamat minimal diisi dengan 20 karakter.',
            'type_kos.required'                     => 'Type kost wajib diisi.',
            'type_owner.required'                   => 'Type pemilik wajib diisi.',
            'resident.required'                     => 'Tipe penghuni kost wajib diisi.',
            'room_size.required'                    => 'ukuran kamar wajib diisi.',
            'year_built.required'                   => 'tahun bangun kost wajib diisi.',
            'number_of_rooms.required'              => 'Jumlah kamar kost wajib diisi.',
            'number_of_empty_rooms.required'        => 'Jumlah kamar kost kosong wajib diisi.',
            'facilities.required'                   => 'Fasilitas wajib diisi.',
            'other_facilities.required'             => 'fasilitas lain wajib diisi.',
            'bathroom.required'                     => 'Kamar mandi wajib diisi.',
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();

        throw new HttpResponseException(
            response()->json([
                'status'        =>'Error',
                'type_message'  => 'array',
                'message'       => $errors,
                'meta'          => null,
                'code'          => 422
            ], 422)
        );
    }

}
