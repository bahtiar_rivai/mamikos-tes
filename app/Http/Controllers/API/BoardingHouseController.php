<?php

namespace App\Http\Controllers\API;

use App\Models\BoardingHouse;
use App\Models\BoardingHousePrice;
use App\Models\BoardingHouseImage;
use App\Http\Controllers\ApiController;
use App\Http\Requests\BoardingHouseRequest;
use App\Http\Requests\BoardingHouseAskRequest;
use App\Http\Resources\BoardingHouseResource;
use Illuminate\Http\Request;
use App\Interfaces\BoardingHouseInterface;

class BoardingHouseController extends ApiController
{

    protected $boardingHouseInterface;

    /**
     * Create a new constructor for this controller
     */
    public function __construct(BoardingHouseInterface $boardingHouseInterface)
    {
        $this->boardingHouseInterface = $boardingHouseInterface;
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {
        return $this->boardingHouseInterface->getAllPaginate($request->all());
    }

    public function show($id)
    {
        return $this->boardingHouseInterface->getBoardingHouseById($id);
    }


    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(BoardingHouseRequest $request)
    {
        return $this->boardingHouseInterface->store($request);
    }

    public function ask(BoardingHouseAskRequest $request)
    {
        return $this->boardingHouseInterface->ask($request);
    }


    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, Product $product)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required',
            'detail' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $product->name = $input['name'];
        $product->detail = $input['detail'];
        $product->save();
        return response()->json([
            "success" => true,
            "message" => "Product updated successfully.",
            "data" => $product
        ]);
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy(Product $product)
    {
        $product->delete();
        return response()->json([
            "success" => true,
            "message" => "Product deleted successfully.",
            "data" => $product
        ]);
    }

}
