<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\OtpRequest;
use App\Http\Requests\RegisterRequest;
use App\Interfaces\UserInterface;

class AuthController extends Controller
{

    protected $userInterface;

    /**
     * Create a new constructor for this controller
     */
    public function __construct(UserInterface $userInterface)
    {
        $this->userInterface = $userInterface;
    }

    /**
     * Register the incoming user.
     *
     * @return Response
     */
    public function register (RegisterRequest $request)
    {
        return $this->userInterface->requestRegister($request);
    }

    /**
     * Login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(LoginRequest $request)
    {
        return $this->userInterface->requestLogin($request);
    }

    /**
     * Submit OTP api
     *
     * @return \Illuminate\Http\Response
     */
    public function submitOtp(OtpRequest $request)
    {
        return $this->userInterface->requestSubmitOTP($request);
    }

}
