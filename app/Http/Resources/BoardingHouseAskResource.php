<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BoardingHouseAskResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                    => $this->id,
            'ask_to'                => $this->ask_to,
            'ask_from'              => $this->ask_from,
            'ask'                   => $this->ask,
            'boarding_house'        => new BoardingHouseResource($this->boardingHouse),
        ];
    }
}
