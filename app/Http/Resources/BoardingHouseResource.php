<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BoardingHouseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                    => $this->id,
            'name'                  => $this->name,
            'address'               => $this->address,
            'type_kos'              => $this->type_kos,
            'type_owner'            => $this->type_owner,
            'resident'              => $this->resident,
            'room_size'             => $this->room_size,
            'year_built'            => $this->year_built,
            'number_of_rooms'       => $this->number_of_rooms,
            'number_of_empty_rooms' => $this->number_of_empty_rooms,
            'facilities'            => $this->facilities,
            'other_facilities'      => $this->other_facilities,
            'bathroom'              => $this->bathroom,
            'prices'                => BoardingHousePricesResource::collection($this->prices),
            'images'                => BoardingHouseImagesResource::collection($this->images),
            'created_at'            => $this->created_at->toDateTimeString(),
            'updated_at'            => $this->updated_at->toDateTimeString(),
        ];
    }
}
