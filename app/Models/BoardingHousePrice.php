<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BoardingHousePrice extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'boarding_house_id',
        'type_price',
        'price',
    ];

    /**
     * Get the boarding house record associated with the image.
     */
    public function boardingHouse()
    {
        return $this->belongsTo('App\Models\BoardingHouse', 'boarding_house_id');
    }
}
