<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BoardingHouse extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'owner_id',
        'name',
        'address',
        'type_kos',
        'type_owner',
        'resident',
        'room_size',
        'year_built',
        'number_of_rooms',
        'number_of_empty_rooms',
        'facilities',
        'other_facilities',
        'bathroom',
    ];

    /**
     * Get the prices for the boarding house price.
     */
    public function prices()
    {
        return $this->hasMany('App\Models\BoardingHousePrice');
    }

    /**
     * Get the images for the boarding house price.
     */
    public function images()
    {
        return $this->hasMany('App\Models\BoardingHouseImage');
    }

}
