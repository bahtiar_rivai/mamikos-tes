<?php

namespace App\Traits;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\LengthAwarePaginator;

trait ApiResponser
{

    protected function successResponse($data, $message = null, $code = 200)
	{
        $meta['message']        = $message;
        $meta['type_message']   = 'string';
        $meta['code']           = $code;
		return response()->json([
			'status'        => true,
			'meta'          => $meta,
			'data'          => $data,
		], $code);
    }

    protected function paginationResponse($data, $meta, $message = null, $code = 200)
	{
        $metaData['message']        = $message;
        $metaData['type_message']   = 'string';
        $metaData['code']           = $code;
        $metaData['current_page']   = $meta->currentPage();
        $metaData['total']          = $meta->count();
        $metaData['per_page']       = $meta->perPage();
        $metaData['last_page']      = $meta->lastPage();
		return response()->json([
			'status'        => true,
			'meta'          => $metaData,
			'data'          => $data,
		], $code);
	}

	protected function errorResponse($message = null, $code)
	{
        $meta['message']        = $message;
        $meta['type_message']   = 'string';
        $meta['code']           = $code;
		return response()->json([
			'status'        => false,
            'meta'          => $meta,
		], $code);
	}

}
