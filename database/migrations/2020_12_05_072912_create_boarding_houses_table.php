<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBoardingHousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boarding_houses', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('owner_id');
            $table->string('name');
            $table->string('address');
            $table->enum('type_kos', ['kos', 'apartemen']);
            $table->enum('type_owner', ['owner', 'administrator']);
            $table->enum('resident', ['female', 'male', 'mix']);
            $table->string('room_size', 50);
            $table->year('year_built');
            $table->smallInteger('number_of_rooms');
            $table->smallInteger('number_of_empty_rooms');
            $table->json('facilities');
            $table->enum('other_facilities', ['isi', 'kosongan']);
            $table->enum('bathroom', ['in', 'out']);
            $table->timestamps();

            $table->foreign('owner_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boarding_houses');
    }
}
