<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBoardingHouseAsksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boarding_house_asks', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('boarding_house_id');
            $table->unsignedBigInteger('ask_to');
            $table->unsignedBigInteger('ask_from');
            $table->text('ask');
            $table->timestamps();

            $table->foreign('boarding_house_id')->references('id')->on('boarding_houses');
            $table->foreign('ask_to')->references('id')->on('users');
            $table->foreign('ask_from')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boarding_house_asks');
    }
}
