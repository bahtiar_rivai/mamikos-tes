<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    { // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        Permission::create(['name' => 'create boarding house']);
        Permission::create(['name' => 'edit boarding house']);
        Permission::create(['name' => 'delete boarding house']);
        Permission::create(['name' => 'ask boarding house']);

        // create roles and assign created permissions

        $role = Role::create(['name' => 'super-admin']);
        $role->givePermissionTo(Permission::all());

        // this can be done as separate statements
        $role = Role::create(['name' => 'regular-user']);
        $role->givePermissionTo(['ask boarding house']);

        // this can be done as separate statements
        $role = Role::create(['name' => 'premium-user']);
        $role->givePermissionTo(['ask boarding house']);

        // or may be done by chaining
        $role = Role::create(['name' => 'owner'])
            ->givePermissionTo(['create boarding house',
                                'edit boarding house',
                                'delete boarding house']);
    }
}
