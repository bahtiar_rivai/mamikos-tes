<?php

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\BoardingHouseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login']);
Route::get('boarding-house', [BoardingHouseController::class, 'index']);
Route::get('boarding-house/{id}', [BoardingHouseController::class, 'show']);
Route::middleware('auth:api')->group( function () {
    Route::post('submit-otp', [AuthController::class, 'submitOtp']);
    Route::group(['middleware' => ['role:owner|super-admin']], function () {
        Route::resource('boarding-house', BoardingHouseController::class)->only([
            'store', 'update', 'destroy'
        ]);
    });
    Route::group(['middleware' => ['role:regular-user|premium-user']], function () {
        Route::post('boarding-house/{id}/ask', [BoardingHouseController::class, 'ask']);
    });
});
